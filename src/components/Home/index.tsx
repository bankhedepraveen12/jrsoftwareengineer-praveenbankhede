import { Col, Row, Image } from "antd";
import React from "react";
import "./home.scss";

const Home = () => {
  return (
    <div className="home">
      <Row justify="space-between" align="middle" className="section-1">
        <Col className="section-1-left" span={24}>
          <img src="/assets/images/blog-posts.png" alt="" className="homeImg" />
        </Col>
      </Row>
    </div>
  );
};

export default Home;
