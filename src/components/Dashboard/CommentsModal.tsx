import React, { useEffect, useState } from "react";
import { Button, Flex, Modal, Spin } from "antd";
import { AiFillMessage } from "react-icons/ai";
import { AppDispatch } from "../../store";
import { CommentsApiCall } from "./components";
import { useDispatch } from "react-redux";
import { apiFailureAction } from "../../commonApiLogic";

interface FuncProps {
  postId: string;
}
interface commentsTYpe {
  postId: Number;
  id: Number;
  name: string;
  email: string;
  body: string;
}
const CommentsModal: React.FC<FuncProps> = ({ postId }) => {
  const dispatch: AppDispatch = useDispatch();

  const [loading, setLoading] = useState<boolean>(false);
  const [count, setCount] = useState<string>("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [comments, setComments] = useState<commentsTYpe[]>([]);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    setLoading(true);
    dispatch(CommentsApiCall(postId))
      .unwrap()
      .then((data) => {
        setComments(data);
        setCount(data?.length.toString());
        setLoading(false);
        // navigate(location.pathname + `?${urlParams.toString()}`);
      })
      .catch((err: Error) => {
        dispatch(apiFailureAction.apiFailure(err));
        setLoading(false);
      });
  }, []);

  return (
    <>
      <div
        onClick={showModal}
        style={{
          width: "15%",
          display: "flex",
          alignItems: "center",
          gap: "5px",
          cursor: "pointer",
        }}
      >
        <AiFillMessage size={24} />
        <span style={{ fontSize: "18px" }}>
          {loading ? <Spin size="small" /> : `${count}`}
        </span>
      </div>

      <Modal
        title="Basic Modal"
        footer={null}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1000}
      >
        <table>
          <thead>
            <tr>
              <th>S.N.</th>
              <th>Post ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>comment</th>
            </tr>
          </thead>
          <tbody>
            {comments.map((item) => (
              <tr key={item.id.toString()}>
                <td>{item.id.toString()}</td>
                <td>{item.postId.toString()}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>{item.body}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </Modal>
    </>
  );
};

export default CommentsModal;
