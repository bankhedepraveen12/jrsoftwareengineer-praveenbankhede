import { Col, Row, Image, Button, Drawer, Menu } from "antd";
import React, { useState } from "react";
import "./navbar.scss";
import { checkIfLogin, getUserName } from "../../utils/sessionManagement";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { IoMenuOutline } from "react-icons/io5";

import { headerUtils, session } from "../../utils";

interface FuncProps {
  setOpen: any;
  setOpenRegisterForm: any;
}
const Header: React.FC<FuncProps> = ({ setOpen, setOpenRegisterForm }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [drawerOpen, setDrawerOpen] = useState(false);

  const onLogout = (): void => {
    session.clearSession();
    onClose();
    navigate("/");
  };

  const showDrawer = (): void => {
    setDrawerOpen(true);
  };

  const onClose = (): void => {
    setDrawerOpen(false);
  };
  return (
    <>
      <Drawer
        placement="left"
        width={"60vw"}
        onClose={onClose}
        open={drawerOpen}
      >
        <Menu mode="vertical" className="drawer-bar">
          <li key="logo">
            <Link to="/dashboard">
              <span className="navbarText" style={{ marginLeft: "10px" }}>
                jrsoftwareengineer
              </span>
            </Link>
          </li>
          <div className="horizontal-line"></div>
          <div className="drawer-main">
            {checkIfLogin() ? (
              <>
                <li key="home">
                  <Link to="/" onClick={onClose} className="drawer-items">
                    <Image
                      src={"/assets/images/LoginProfile.png"}
                      alt="pic"
                      preview={false}
                    />
                  </Link>
                </li>
                <li key="about">
                  <span onClick={onClose} className="drawer-items">
                    {getUserName()?.length > 16
                      ? getUserName().slice(0, 12) + "..."
                      : getUserName()}
                  </span>
                </li>
                <li key="projects">
                  <span
                    onClick={() => {
                      onLogout();
                    }}
                    className="drawer-items"
                  >
                    Logout
                  </span>
                </li>
              </>
            ) : (
              <>
                <li key="projects">
                  <span
                    onClick={() => {
                      setOpen(true);
                      onClose();
                    }}
                    className="drawer-items"
                  >
                    Login
                  </span>
                </li>
                <li key="projects">
                  <span
                    onClick={() => {
                      setOpenRegisterForm(true);
                      setOpen(true);
                      onClose();
                    }}
                    className="drawer-items"
                  >
                    Register
                  </span>
                </li>
              </>
            )}
          </div>
        </Menu>
      </Drawer>
      <Menu mode="horizontal" className="navbar">
        <IoMenuOutline
          size={40}
          className="mune-icon"
          onClick={() => showDrawer()}
        />

        <li key="logo">
          <Link to="/dashboard">
            <span className="navbarText" style={{ marginLeft: "10px" }}>
              jrsoftwareengineer
            </span>
          </Link>
        </li>
        <div className="nav-main">
          {checkIfLogin() ? (
            <>
              <li key="home">
                <Image
                  src={"/assets/images/LoginProfile.png"}
                  alt="pic"
                  preview={false}
                />
              </li>
              <li key="about">
                <Link to={"/about"} className="nav-items">
                  {getUserName()?.length > 16
                    ? getUserName().slice(0, 12) + "..."
                    : getUserName()}
                </Link>
              </li>

              <li key="wallet" style={{ marginLeft: "2%" }}>
                <Button className={`nav-btn`} onClick={() => onLogout()}>
                  Logout
                </Button>
              </li>
            </>
          ) : (
            <>
              <li key="wallet" style={{ marginLeft: "2%" }}>
                <Button className={`nav-btn`} onClick={() => setOpen(true)}>
                  Login
                </Button>
              </li>
              <li key="wallet" style={{ marginLeft: "2%" }}>
                <Button
                  className={`nav-btn`}
                  onClick={() => {
                    setOpenRegisterForm(true);
                    setOpen(true);
                  }}
                >
                  Register
                </Button>
              </li>
            </>
          )}
        </div>
      </Menu>
    </>
  );
};
export default Header;
